import React, { Component } from 'react';
import './App.css';
import axios from 'axios';
const openpgp = require('openpgp');

class KbUser extends Component {
  constructor (props){
    super(props);

    this.state = {
      userName: '',
      buttonLabel: 'Fetch pubkey',
      buttonDisabled: true,
      pubKey: null
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({
      userName: event.target.value,
      buttonLabel: 'Fetch pubkey',
      buttonDisabled: false
    });
  }

  handleSubmit(event) {
    this.setState({
      buttonLabel: 'Fetching...',
      buttonDisabled: true
    });
    axios.get('https://keybase.io/' + this.state.userName + '/pgp_keys.asc')
    .then(({ data }) => {
      this.setState({buttonLabel: 'Reading...'});
      openpgp.key.readArmored(data).then(({ keys }) => {
        this.props.updatePubKey(keys[0]);
        this.setState({
          buttonLabel: 'KeyID: ' + keys[0].primaryKey.getKeyId().toHex()
        });
      });
    })
    .catch((error) => {
      if (error.response) {
        this.setState({buttonLabel: error.response.status});
      } else {
        this.setState({buttonLabel: error.message});
      };
    });
    event.preventDefault();
  }

  render() {
    return (
      <div>
        <h2>Recipient</h2>
        <form onSubmit={this.handleSubmit}>
          <input
            type="text"
            value={this.state.userName}
            placeholder="Keybase username"
            onChange={this.handleChange}
          />
          <input
            type="submit"
            value={this.state.buttonLabel}
            disabled={this.state.buttonDisabled}
          />
        </form>
      </div>
    );
  }
}

class Encryptor extends Component {
  constructor (props){
    super(props);

    this.state = {
      clearText: '',
      encryptedText: '',
      buttonDisabled: true,
      pubKey: null
    }

    this.setButtonDisabled = this.setButtonDisabled.bind(this);
    this.setPubKey = this.setPubKey.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  setButtonDisabled() {
    var x = true;
    if (this.state.clearText.length > 0 && this.state.pubKey) {
      x = false;
    }
    this.setState({buttonDisabled: x});
  }

  setPubKey(key) {
    this.setState({pubKey: key});
    this.setButtonDisabled();
  }

  handleChange(event) {
    this.setState({clearText: event.target.value});
    this.setButtonDisabled();
  }

  handleSubmit(event) {
    this.setState({buttonDisabled: true});
    var options = {
      message: openpgp.message.fromText(this.state.clearText),
      publicKeys: this.state.pubKey
    };
    openpgp.encrypt(options).then((cipherText) => {
      this.setState({encryptedText: cipherText.data});
    });
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <h2>Message</h2>
        <p>
          <textarea
            value={this.state.clearText}
            placeholder="clear text"
            onChange={this.handleChange}
          />
        </p>
        <input
          type="submit"
          value="PGP Encrypt"
          disabled={this.state.buttonDisabled}
        />
        <p>
          <textarea
            value={this.state.encryptedText}
            placeholder="encrypted text"
            readOnly
            className="encrypted"
          />
        </p>
      </form>
    );
  }
}

class App extends Component {
  updatePubKey(key) {
    this.refs.EncryptorComponent.setPubKey(key);
  }

  render() {
    return (
      <React.Fragment>
        <h1>Encryptor on browser</h1>
        <KbUser updatePubKey={this.updatePubKey.bind(this)} />
        <Encryptor ref='EncryptorComponent' />
        <div className="footer">
          <p>
            Powered by <a href="https://openpgpjs.org/">OpenPGP.js</a>.
            Fork me on <a href="https://gitlab.com/zunda/encryptor-on-browser">GItLab</a>!</p>
        </div>
      </React.Fragment>
    );
  }
}

export default App;
